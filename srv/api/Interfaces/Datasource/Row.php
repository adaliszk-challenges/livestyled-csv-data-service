<?php
namespace UserApi\Interfaces\Datasource;

interface Row
{
    public function getData(): iterable;
    public function get(int $idx);
}